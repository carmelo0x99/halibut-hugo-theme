Halibut ![A stylized halibut](static/images/Freepik/halibut32.png "Halibut") Hugo theme, based on [Hugo Mini Course](https://hugo-mini-course.netlify.app/) and [Bulma](https://bulma.io/).</br>
The _halibut_ icon is based on [Halibut icons created by Freepik](https://www.flaticon.com/free-icons/halibut).

----

### Why
> Yes. I chose him out of thousands. I didn't like the others, they were all too flat.

Plenty of beautiful [Hugo Themes](https://themes.gohugo.io/) out there, so, why did I have to create _mine_?</br>

Quoting from Monty Python, they were all too f(l)at for me. What I wanted was a theme that I could fully understand, manipulate, and customize. That's why I chose to build mine from scratch.</br>

----

### How the theme was created from scratch
1. create a new repo on Gitlab
2. clone the repo locally, `cd` into it
3. run the following commands, in sequence:
```
$ hugo new theme .
$ mv themes/* .
$ rmdir public resources themes
```

----

### How to use the theme
1. generate a brand new Hugo site:
```
$ hugo new site <Hugo-site-name> && cd $_
```

Two options are available as the method to import a theme into the newly generated site:
- A. `git submodule`
- B. `Hugo modules`

2. Option **A** - `git submodule`:<br/>
- A1. initialize the directory as a git repository:
```
(<Hugo-site-name>) $ git init
```

- A2. add the external theme as a `git submodule`:
```
(<Hugo-site-name>) $ git submodule add https://github.com/<user>/<repository> themes/<theme_name>
```

- A3. add the theme to the configuration file:
```
(<Hugo-site-name>) $ echo "theme = '<theme_name>'" >> hugo.toml
```

2. Option **B** - `Hugo modules`:<br/>
- B1. initialize the site as a [Hugo Module](https://gohugo.io/hugo-modules/use-modules/):
```
(<Hugo-site-name>) $ hugo mod init example.org/user/repository
```

- B2. add the following to **either** `hugo.toml`:
```
[module]
[[module.imports]]
  path = "gitlab.com/carmelo0x99/halibut-hugo-theme"
```

... **or** `hugo.yaml`:
```
module:
  imports:
  - path: gitlab.com/carmelo0x99/halibut-hugo-theme
```

3. add some basic contents to your site:
```
(<Hugo-site-name>) $ hugo new about.md
(<Hugo-site-name>) $ hugo new posts/_index.md
(<Hugo-site-name>) $ hugo new snippets/_index.md
```

4. [`serve`](https://gohugo.io/commands/hugo_server/)
```
(<Hugo-site-name>) $ hugo server -D
```

----

### How to update the theme
- option A
```
(<Hugo-site-name>) $ git submodule update --remote --merge
```

- option B<br/>
see [Update Modules](https://gohugo.io/hugo-modules/use-modules/#update-modules)

